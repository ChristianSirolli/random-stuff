/* returns values of selected HTMLOptionElements in an HTMLSelectElement */
HTMLSelectElement.prototype.selectedValues = function () {
    return [...this.selectedOptions].map(option => option.value);
}

/* returns indexes of selected HTMLOptionElements in an HTMLSelectElement */
HTMLSelectElement.prototype.selectedIndexes = function () {
    return [...this.selectedOptions].map(option => option.index);
}
